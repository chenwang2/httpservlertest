package com.chenwang;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

public class MyHandler implements HttpHandler {
    private String method;

    public MyHandler() {
    }

    public MyHandler(String method) {
        this.method = method;
    }

    @Override
    public void handle(HttpExchange he) throws IOException {
        String requestMethod = he.getRequestMethod();
        //时间有限  只处理get请求
        if (requestMethod.equals("GET")) {
            //响应头
            Headers responseHeaders = he.getResponseHeaders();
            responseHeaders.set("Content-Type", "text/html;charset=utf-8");
            he.sendResponseHeaders(200, 0);
            // parse request
            OutputStream responseBody = he.getResponseBody();
            //请求头
            Headers requestHeaders = he.getRequestHeaders();
            try {
                Map<String, String> map = urlSplit(he.getRequestURI().getRawQuery());
                Integer result = calculation(map);
                responseBody.write(result.toString().getBytes());
            } catch (Exception e) {
                responseBody.write("请输入规定的地址哦".getBytes());
            } finally {
                responseBody.close();
            }
        }
    }

    //计算
    public Integer calculation(Map<String, String> map) {
        int a = Integer.parseInt(map.get("a"));
        int b = Integer.parseInt(map.get("b"));
        if (this.method.equals("add")) {
            return a + b;
        } else if (this.method.equals("mult")) {
            return a * b;
        }
        return null;
    }

    //截取参数放到map中 需要传入地址“？”后面的东西   2021  陈旺
    public static Map<String, String> urlSplit(String strUrlParam) {
        Map<String, String> mapRequest = new HashMap<String, String>();
        String[] arrSplit = null;

        if (strUrlParam == null) {
            return mapRequest;
        }
        arrSplit = strUrlParam.split("[&]");
        for (String strSplit : arrSplit) {
            String[] arrSplitEqual = null;
            arrSplitEqual = strSplit.split("[=]");
            //解析出键值
            if (arrSplitEqual.length > 1) {
                //正确解析
                mapRequest.put(arrSplitEqual[0], arrSplitEqual[1]);
            } else {
                if (arrSplitEqual[0] != "") {
                    //只有参数没有值，不加入
                    mapRequest.put(arrSplitEqual[0], "");
                }
            }
        }
        return mapRequest;
    }

}
