package com.chenwang;

import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Test {
    public static void main(String[] args) throws IOException {
        HttpServer httpServer = HttpServer.create(new InetSocketAddress(80), 0);
        httpServer.createContext("/add", new MyHandler("add"));
        httpServer.createContext("/mult", new MyHandler("mult"));
        httpServer.setExecutor(Executors.newFixedThreadPool(10));
        httpServer.start();
    }


}
